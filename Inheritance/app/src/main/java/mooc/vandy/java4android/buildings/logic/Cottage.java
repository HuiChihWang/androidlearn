package mooc.vandy.java4android.buildings.logic;

/**
 * This is the cottage class file.  It is a subclass of House.
 */
public class Cottage extends House {

    private boolean mSecondFloor = false;

    Cottage(int dimension, int lotLength, int lotWidth) {
        super(dimension, dimension, lotLength, lotWidth);
    }

    Cottage(int dimension, int lotLength, int lotWidth, String owner, boolean secondFloor) {
        super(dimension, dimension, lotLength, lotWidth, owner);
        mSecondFloor = secondFloor;
    }

    boolean hasSecondFloor() {
        return mSecondFloor;
    }

    @Override
    public String toString() {
        StringBuilder strBuild = new StringBuilder(super.toString());
        if (hasSecondFloor()) {
            strBuild.append("; is a two story cottage");
        } else {
            strBuild.append("; is a cottage");
        }

        return strBuild.toString();
    }
}

