package mooc.vandy.java4android.buildings.logic;

/**
 * This is the Building class file.
 */
public class Building {

    private int mLength;
    private int mWidth;
    private int mLotLength;
    private int mLotWidth;

    Building(int iLength, int iWidth, int iLotLength, int iLotWidth) {
        mLength = iLength;
        mWidth = iWidth;
        mLotLength = iLotLength;
        mLotWidth = iLotWidth;
    }

    int getLength() {
        return mLength;
    }

    int getWidth() {
        return mWidth;
    }

    int getLotLength() {
        return mLotLength;
    }

    int getLotWidth() {
        return mLotWidth;
    }

    void setLength(int iLength) {
        mLength = iLength;
    }

    void setWidth(int iWidth) {
        mWidth = iWidth;
    }

    void setLotLength(int iLength) {
        mLotLength = iLength;
    }

    void setLotWidth(int iWidth) {
        mLotWidth = iWidth;
    }

    int calcBuildingArea() {
        return mLength * mWidth;
    }

    int calcLotArea() {
        return mLotWidth * mLotLength;
    }


    @Override
    public String toString() {
        return "";
    }
}
