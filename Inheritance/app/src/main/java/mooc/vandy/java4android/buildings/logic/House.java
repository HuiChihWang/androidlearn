package mooc.vandy.java4android.buildings.logic;

import java.util.Locale;

/**
 * This is the House class file that extends Building.
 */
public class House extends Building {
    private String mOwner = null;
    private boolean mPool = false;

    House(int iLength, int iWidth, int iLotLength, int iLotWidth) {
        super(iLength, iWidth, iLotLength, iLotWidth);
    }

    House(int iLength, int iWidth, int iLotLength, int iLotWidth, String strOwner) {
        super(iLength, iWidth, iLotLength, iLotWidth);
        mOwner = strOwner;

    }

    House(int iLength, int iWidth, int iLotLength, int iLotWidth, String strOwner, boolean bHasPool) {
        super(iLength, iWidth, iLotLength, iLotWidth);
        mOwner = strOwner;
        mPool = bHasPool;
    }

    String getOwner() {
        return mOwner;
    }

    boolean hasPool() {
        return mPool;
    }

    void setOwner(String strOwner) {
        mOwner = strOwner;
    }

    void setPool(boolean bPool) {
        mPool = bPool;
    }

    //TODO
    @Override
    public String toString() {
        String strOwner = mOwner == null ? "n/a" : mOwner;

        StringBuilder strBuild = new StringBuilder();
        strBuild.append(String.format(Locale.ENGLISH, "Owner: %s",
                getOwner() == null ? "n/a" : getOwner()));

        if (hasPool()) {
            strBuild.append("; has a pool");
        }

        if (calcLotArea() > calcBuildingArea()){
            strBuild.append("; has a big open space");
        }

        return strBuild.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }

        House build = (House) obj;

        return calcBuildingArea() == build.calcBuildingArea() && hasPool() == build.hasPool();
    }
}
