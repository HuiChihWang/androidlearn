package mooc.vandy.java4android.buildings.logic;

import java.util.Locale;

/**
 * This is the office class file, it is a subclass of Building.
 */
public class Office extends Building {

    private String mBusinessName = null;
    private int mParkingSpaces = 0;
    private static int sTotalOffices = 0;

    Office(int length, int width, int lotLength, int lotWidth) {
        super(length, width, lotLength, lotWidth);
        sTotalOffices += 1;
    }

    Office(int length, int width, int lotLength, int lotWidth, String strName) {
        this(length, width, lotLength, lotWidth);
        mBusinessName = strName;
    }

    Office(int length, int width, int lotLength, int lotWidth, String strName, int spaces) {
        this(length, width, lotLength, lotWidth, strName);
        mParkingSpaces = spaces;
    }

    String getBusinessName() {
        return mBusinessName;
    }

    int getParkingSpaces() {
        return mParkingSpaces;
    }

    void setBusinessName(String mBusinessName) {
        this.mBusinessName = mBusinessName;
    }

    void setParkingSpaces(int mParkingSpaces) {
        this.mParkingSpaces = mParkingSpaces;
    }

    @Override
    public String toString() {
        StringBuilder strBuild = new StringBuilder();
        strBuild.append(String.format(Locale.ENGLISH, "Business: %s",
                getBusinessName() == null ? "unoccupied" : getBusinessName()));

        if (getParkingSpaces() != 0) {
            strBuild.append(String.format(Locale.ENGLISH, "; has %d parking spaces", getParkingSpaces()));
        }

        strBuild.append(String.format(Locale.ENGLISH, " (total offices: %d)", sTotalOffices));

        return strBuild.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Office office = (Office) obj;

        return office.calcBuildingArea() == calcBuildingArea() && office.getParkingSpaces() == getParkingSpaces();
    }
}
