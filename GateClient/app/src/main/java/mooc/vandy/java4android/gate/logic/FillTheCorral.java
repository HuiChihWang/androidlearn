package mooc.vandy.java4android.gate.logic;

import java.util.Locale;
import java.util.Random;

import mooc.vandy.java4android.gate.ui.OutputInterface;

/**
 * This class uses your Gate class to fill the corral with snails.  We
 * have supplied you will the code necessary to execute as an app.
 * You must fill in the missing logic below.
 */
public class FillTheCorral {
    /**
     * Reference to the OutputInterface.
     */
    private OutputInterface mOut;

    /**
     * Constructor initializes the field.
     */
    FillTheCorral(OutputInterface out) {
        mOut = out;
    }

    // TODO -- Fill your code in here
    public void setCorralGates(Gate[] gates, Random selectDirection) {
        for (Gate gate : gates) {
            gate.setSwing(selectDirection.nextInt(3) - 1);
        }
    }

    public boolean anyCorralAvailable(Gate[] gates) {
        for (Gate gate : gates) {
            if (gate.getSwingDirection() == gate.IN) {
                return true;
            }
        }
        return false;
    }

    public int corralSnails(Gate[] corrals, Random rand) {
        int iTotalOut = 5;
        int iSimCount = 0;
        while (iTotalOut > 0 && iSimCount < 100) {
            int selectGateIdx = rand.nextInt(corrals.length);
            int randCount = rand.nextInt(iTotalOut) + 1;
            iTotalOut -= corrals[selectGateIdx].thru(randCount);
            printSteps(selectGateIdx, randCount);
            iSimCount += 1;
        }

        printDoorState(corrals);


        return iSimCount;
    }

    private void printDoorState(Gate[] gates) {
        for (Gate gate : gates) {
            mOut.println(gate.toString());
        }
    }

    private void printSteps(int selectIdx, int randCount) {
        mOut.println(String.format(Locale.ENGLISH, "%d are trying to move through corral %d\n",
                randCount, selectIdx));
    }
}
