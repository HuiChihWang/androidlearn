package mooc.vandy.java4android.gate.logic;

import android.widget.HeaderViewListAdapter;

import java.util.Locale;
import java.util.Random;

import mooc.vandy.java4android.gate.ui.OutputInterface;

/**
 * This class uses your Gate class to manage a herd of snails.  We
 * have supplied you will the code necessary to execute as an app.
 * You must fill in the missing logic below.
 */
public class HerdManager {
    /**
     * Reference to the output.
     */
    private OutputInterface mOut;

    /**
     * The input Gate object.
     */
    private Gate mEastGate;

    /**
     * The output Gate object.
     */
    private Gate mWestGate;

    /**
     * Maximum number of iterations to run the simulation.
     */
    public static final int HERD = 24;
    private static final int MAX_ITERATIONS = 10;

    /**
     * Constructor initializes the fields.
     */
    public HerdManager(OutputInterface out,
                       Gate westGate,
                       Gate eastGate) {
        mOut = out;

        mWestGate = westGate;
        mWestGate.open(Gate.IN);

        mEastGate = eastGate;
        mEastGate.open(Gate.OUT);
    }

    void simulateHerd(Random randGen) {
        int iTotalSnailsIn = HERD;
        mOut.println(String.format(Locale.ENGLISH, "There are currently %d snails in the pen and %d snails in the pasture",
                iTotalSnailsIn, HERD - iTotalSnailsIn));
        for (int iIter = 0; iIter < MAX_ITERATIONS; ++iIter) {
            iTotalSnailsIn += GenerateRandomFlow(iTotalSnailsIn, randGen);
            mOut.println(String.format(Locale.ENGLISH, "There are currently %d snails in the pen and %d snails in the pasture",
                    iTotalSnailsIn, HERD - iTotalSnailsIn));
        }
    }

    private int GenerateRandomFlow(int iTotalIn, Random randGen) {
        if (iTotalIn == 0) {
            return mWestGate.thru(randGen.nextInt(HERD - iTotalIn)+1);
        } else if (iTotalIn == HERD) {
            return mEastGate.thru(randGen.nextInt(iTotalIn)+1);
        } else {
            return randGen.nextBoolean() ?
                    mEastGate.thru(randGen.nextInt(iTotalIn)+1) :
                    mWestGate.thru(randGen.nextInt(HERD - iTotalIn)+1);
        }
    }


}
