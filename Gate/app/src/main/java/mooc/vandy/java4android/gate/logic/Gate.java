package mooc.vandy.java4android.gate.logic;

/**
 * This file defines the Gate class.
 */
public class Gate {
    // TODO -- Fill in your code here

    public static final int IN = 1;
    public static final int OUT = -1;
    public static final int CLOSED = 0;

    private int mSwing = 0;

    public Gate() {

    }

    public boolean setSwing(int direction) {
        if (!IsStateValid(direction)) {
            return false;
        }

        mSwing = direction;
        return true;
    }

    public boolean open(int direction) {
        if (direction == CLOSED) {
            return false;
        }
        return setSwing(direction);
    }

    public void close() {
        setSwing(CLOSED);
    }

    public int getSwingDirection() {
        return mSwing;
    }

    public int thru(int count) {
        int changeSign = 0;
        switch (getSwingDirection()) {
            case IN: {
                changeSign = 1;
                break;
            }
            case OUT: {
                changeSign = -1;
                break;
            }
            default: {
                break;
            }

        }
        return changeSign * count;
    }

    @Override
    public String toString() {
        switch (getSwingDirection()) {
            case IN: {
                return "This gate is open and swings to enter the pen only";
            }
            case OUT: {
                return "This gate is open and swings to exit the pen only";
            }
            case CLOSED: {
                return "This gate is closed";
            }
            default: {
                return "This gate has an invalid swing direction";
            }

        }
    }

    private boolean IsStateValid(int direction) {
        return direction == IN || direction == OUT || direction == CLOSED;
    }
}
