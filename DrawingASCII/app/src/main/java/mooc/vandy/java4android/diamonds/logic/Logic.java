package mooc.vandy.java4android.diamonds.logic;

import android.util.Log;

import mooc.vandy.java4android.diamonds.ui.OutputInterface;

/**
 * This is where the logic of this App is centralized for this assignment.
 * <p>
 * The assignments are designed this way to simplify your early
 * Android interactions.  Designing the assignments this way allows
 * you to first learn key 'Java' features without having to beforehand
 * learn the complexities of Android.
 */
public class Logic
        implements LogicInterface {
    /**
     * This is a String to be used in Logging (if/when you decide you
     * need it for debugging).
     */
    public static final String TAG = Logic.class.getName();

    /**
     * This is the variable that stores our OutputInterface instance.
     * <p>
     * This is how we will interact with the User Interface [MainActivity.java].
     * <p>
     * It is called 'out' because it is where we 'out-put' our
     * results. (It is also the 'in-put' from where we get values
     * from, but it only needs 1 name, and 'out' is good enough).
     */
    private OutputInterface mOut;

    /**
     * This is the constructor of this class.
     * <p>
     * It assigns the passed in [MainActivity] instance (which
     * implements [OutputInterface]) to 'out'.
     */
    public Logic(OutputInterface out) {
        mOut = out;
    }

    /**
     * This is the method that will (eventually) get called when the
     * on-screen button labeled 'Process...' is pressed.
     */
    public void process(int size) {
        printBoundary(size);
        printTriangle(size);
        printBoundary(size);
    }

    private void printBoundary(int iSize) {
        mOut.print('+');
        for (int iIdx = 0; iIdx < 2 * iSize; ++iIdx) {
            mOut.print('-');
        }
        mOut.println('+');
    }

    private void printTriangle(int iSize) {
        for (int iLine = 0; iLine < 2 * iSize - 1; ++iLine) {
            printEachLine(iLine, iSize);
            mOut.println();
        }
    }

    private void printEachLine(int iLine, int iSize) {
        mOut.print('|');

        int iPosSlashIdx = iSize - iLine - 1;
        int iNegSlashIdx = iPosSlashIdx + iLine * 2 + 1;

        if (iLine >= iSize) {
            iPosSlashIdx += (2 * iSize - 1);
            iNegSlashIdx -= (2 * iSize - 1);
        }

        int iLeftIdx = Math.min(iPosSlashIdx, iNegSlashIdx);
        int iRightIdx = Math.max(iPosSlashIdx, iNegSlashIdx);

        char cContent = iLine % 2 == 0 ? '=' : '-';
        char cPosSlash = iLine == iSize - 1 ? '<' : '/';
        char cNegSlash = iLine == iSize - 1 ? '>' : '\\';


        for (int iIdx = 0; iIdx < 2 * iSize; ++iIdx) {
            if (iIdx == iPosSlashIdx) {
                mOut.print(cPosSlash);
            } else if (iIdx == iNegSlashIdx) {
                mOut.print(cNegSlash);
            } else if (iIdx > iLeftIdx && iIdx < iRightIdx) {
                mOut.print(cContent);
            } else {
                mOut.print(' ');
            }
        }

        mOut.print('|');
    }


}
